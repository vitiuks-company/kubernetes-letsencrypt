FROM fedora:24
MAINTAINER Andrew Vitiuk <andruwa13@me.com>

RUN dnf install certbot -y && dnf clean all
RUN mkdir -p /etc/letsencrypt/live/kubernetes-letsencrypt.romania-pasport.com

COPY secret-patch-template.json /
COPY deployment-patch-template.json /
COPY entrypoint.sh /

CMD ["/entrypoint.sh"]
